// A library for writing UfugeWM plugins in Go.
// UfugeWM has an architecture where there's multiple plugins and one main server which allows them to interact.
// The plugins are connected with the server via UNIX sockets.
// The plugins may implement an arbitrary number of interfaces, which represent a function in the window manager.
// The connection is done over either JSON-RPC, or the custom protobuf version, which is quite similar to it.
package ufugewm

import (
	"io"
	"net"
)

const addr = "/var/run/ufugewm.sock"

// A connection to the UfugeWM server.
type Conn net.Conn

func Dial() (Conn, error) {
	DialWithAddress(addr)
}

func DialWithAddress(address string) (Conn, error) {
	DialWithProto("unix", address)
}

func DialWithProto(proto, address string) (Conn, error) {
	conn, err := net.Dial("unix, ")
}

func ReadMsg(r io.Reader) ([]byte, error) {
	numlength := make([]byte, 1)
	_, err := r.Read(numlength)
	if err != nil {
		return nil, err
	}
	length := make([]byte, numlength[0])
	_, err := r.Read(length)
	if err != nil {
		return nil, err
	}
	intlength := binary.LittleEndian.Uint64(length)
	msg := make([]byte, intlength)
	_, err := r.Read(msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}